package fr.ricarddignac.android;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class AikidoReignacActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://artsmartiauxreignacais.jimdo.com"));
        startActivity(browserIntent);
        finish();
    }
}